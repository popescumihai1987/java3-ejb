This is a java 3 - EJB project for a class at Telecom Academy.

It uses a queue to create a user in the database - with the Register button in the index page.

After creation of the user the login is possible with Login button and a web service endpoint.

After login we have our user page where we can add, delete and view the books we have stored for our user.

The details are not so well adjusted and it could very well be more complex.