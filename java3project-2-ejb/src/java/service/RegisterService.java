package service;

import dao.UserDAO;
import db.UserDB;
import dto.UserDTO;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class RegisterService {
    
    @EJB
    private UserDAO userDao;
    
    public void register(UserDTO userDto) {
        UserDB userDB = userDao.findUser(userDto.getUsername());
        if(userDB == null) {
            userDB = new UserDB();
            userDB.setUsername(userDto.getUsername());
            userDB.setPassword(userDto.getPassword());
            userDao.addUser(userDB);
        }
    }
}
