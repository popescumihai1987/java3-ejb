package dao;

import db.UserDB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class UserDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    public UserDB findUser(String username){
        Query query = em.createNamedQuery("UserDB.findByUsername");
        System.out.println("#########################");
        System.out.println(username);
        query.setParameter("username", username);
        System.out.println(query);
        System.out.println("#########################");
        try{
            return (UserDB) query.getSingleResult();
        } catch (Exception e){
            System.out.println("#########################");
            return null;
        }
    }
    
    public void addUser(UserDB user){
        em.persist(user);
    }
    
}
