package mdb;

import dto.UserDTO;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import service.RegisterService;


@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/MyQueue")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class UserRegisterListener implements MessageListener {
    
    @EJB
    private RegisterService service;

    @Override
    public void onMessage(Message message) {
        try{
            UserDTO user = message.getBody(UserDTO.class);
            System.out.println(user.toString());
            service.register(user);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    
}
