CREATE DATABASE IF NOT EXISTS java3projectdb;

CREATE USER 'java3projectuser'@'localhost' IDENTIFIED BY 'java3projectpassword';

GRANT ALL PRIVILEGES ON * . * TO 'java3projectuser'@'localhost';

FLUSH PRIVILEGES;

USE java3projectdb;

CREATE TABLE users (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(30) NOT NULL,
password VARCHAR(30) NOT NULL
);

CREATE TABLE books (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(30) NOT NULL,
author VARCHAR(30) NOT NULL,
commentary VARCHAR(200) NOT NULL,
user_id INT(6) UNSIGNED,
CONSTRAINT FK_UserBook FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
);

#drop database java3projectdb;