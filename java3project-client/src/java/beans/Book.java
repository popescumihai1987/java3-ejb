/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.xml.ws.WebServiceRef;
import ws.BookDB;
import ws.MainServiceWSService;

@Named
@SessionScoped
public class Book implements Serializable{
    
    @WebServiceRef(wsdlLocation = "http://localhost:8080/MainServiceWSService/MainServiceWS?wsdl")
    private MainServiceWSService service;

    private String title;
    private String author;
    private String commentary;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }
    
    public List<BookDB> callGetBooksWS(java.lang.String username) {
        ws.MainServiceWS port = service.getMainServiceWSPort();
        return port.getBooks(username);
    }
    
    public boolean callDeleteBookWS(java.lang.String title) {
        ws.MainServiceWS port = service.getMainServiceWSPort();
        return port.deleteBook(title);
    }
    
    public boolean callAddBookWS(String title, String author, String comm, String user) {
        ws.MainServiceWS port = service.getMainServiceWSPort();
        return port.addBook(title, author, comm, user);
    }
}
