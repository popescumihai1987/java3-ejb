/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import dto.UserDTO;
import java.io.Serializable;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.xml.ws.WebServiceRef;
import ws.MainServiceWSService;

@Named
@SessionScoped
public class User implements Serializable{

    @WebServiceRef(wsdlLocation = "http://localhost:8080/MainServiceWSService/MainServiceWS?wsdl")
    private MainServiceWSService service;

    @Resource(mappedName = "jms/MyQueue")
    private Queue myQueue;

    @Inject
    @JMSConnectionFactory("jms/__defaultConnectionFactory")
    private JMSContext context;
    
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public void register(String username, String password) {
        UserDTO user = new UserDTO();
        user.setUsername(username);
        user.setPassword(password);
        context.createProducer().send(myQueue, user);
    }
    
    private void sendJMSMessageToMyQueue(String messageData) {
        context.createProducer().send(myQueue, messageData);
    }

    public String callLoginWS(java.lang.String username, java.lang.String password) {
        ws.MainServiceWS port = service.getMainServiceWSPort();
        port.login(username, password);
        return username;
    }
    
}
