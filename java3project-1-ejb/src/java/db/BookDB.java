package db;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "books")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BookDB.findAll", query = "SELECT b FROM BookDB b")
    , @NamedQuery(name = "BookDB.findById", query = "SELECT b FROM BookDB b WHERE b.id = :id")
    , @NamedQuery(name = "BookDB.findByUserId", query = "SELECT b FROM BookDB b WHERE b.userId.id = :id")
    , @NamedQuery(name = "BookDB.findByTitle", query = "SELECT b FROM BookDB b WHERE b.title = :title")
    , @NamedQuery(name = "BookDB.findByAuthor", query = "SELECT b FROM BookDB b WHERE b.author = :author")
    , @NamedQuery(name = "BookDB.findByCommentary", query = "SELECT b FROM BookDB b WHERE b.commentary = :commentary")
    , @NamedQuery(name = "BookDB.deleteByTitle", query = "DELETE FROM BookDB b WHERE b.title = :title")})
public class BookDB implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "author")
    private String author;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "commentary")
    private String commentary;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne
    private UserDB userId;

    public BookDB() {
    }

    public BookDB(Integer id) {
        this.id = id;
    }

    public BookDB(Integer id, String title, String author, String commentary) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.commentary = commentary;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    public UserDB getUserId() {
        return userId;
    }

    public void setUserId(UserDB userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookDB)) {
            return false;
        }
        BookDB other = (BookDB) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.BookDB[ id=" + id + " ]";
    }
    
}
