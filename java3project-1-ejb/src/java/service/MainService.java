
package service;

import dao.UserDAO;
import db.UserDB;
import dto.UserDTO;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import remote.IMainService;

@Stateless
@LocalBean
public class MainService implements IMainService {
    
    @EJB
    private UserDAO userDAO;

    @Override
    public void register(UserDTO user) {
        System.out.println(user.getUsername());
    }    
    
    public boolean login(String username, String password) {
        UserDB user = userDAO.findUser(username);
        if (user != null && user.getPassword().equals(password)) {
            return true;
        }
        return false;
    }
}
