package dao;

import db.BookDB;
import db.UserDB;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class BookDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    @EJB
    private UserDAO userDAO;
    
    public List<BookDB> findBooks(String username){
        UserDB user = userDAO.findUser(username);
        Query query = em.createNamedQuery("BookDB.findByUserId");
        query.setParameter("id", user.getId());
        List<BookDB> rs = query.getResultList();
        try{
            return rs;
        } catch (Exception e){
            return null;
        }
    }

    public boolean deleteBook(String title) {
        Query query = em.createNamedQuery("BookDB.deleteByTitle");
        query.setParameter("title", title);
        try{
            query.executeUpdate();            
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public boolean addBook(String title, String author, String comm, String username) {
        UserDB user = userDAO.findUser(username);
        Query query = em.createNativeQuery("INSERT INTO books (title, author, commentary, user_id) VALUES (?,?,?,?)")
                .setParameter(1, title)
                .setParameter(2, author)
                .setParameter(3, comm)
                .setParameter(4, user.getId());
        try{
            query.executeUpdate();            
            return true;
        } catch (Exception e){
            return false;
        }
    }
    
}
