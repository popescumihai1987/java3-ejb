
package ws;

import dao.BookDAO;
import dao.UserDAO;
import db.BookDB;
import db.UserDB;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import service.MainService;

@WebService
@Stateless
public class MainServiceWS {
    
    @EJB
    private MainService service;
    
    @EJB
    private UserDAO userDAO;
    
    @EJB
    private BookDAO bookDAO;
    
    @WebMethod
    public UserDB login(
            @WebParam String username, 
            @WebParam String password) {
        UserDB user = userDAO.findUser(username);
        System.out.println("LOGIN");
        return user;
    }
    
    @WebMethod
    public List<BookDB> getBooks(
            @WebParam String username) {
        List<BookDB> books = bookDAO.findBooks(username);
        System.out.println("BOOKS");
        return books;
    }
    
    @WebMethod
    public boolean deleteBook(
            @WebParam String title) {
        return bookDAO.deleteBook(title);
    }
    
    @WebMethod
    public boolean addBook(
            @WebParam String title,
            @WebParam String author,
            @WebParam String comm,
            @WebParam String user) {
        return bookDAO.addBook(title, author, comm, user);
    }
    
}
